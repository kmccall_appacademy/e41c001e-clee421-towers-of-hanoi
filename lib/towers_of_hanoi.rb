  # Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    #Previously had it set to display @ 1, 2, 3 but spec was strict
    #about 3, 2, 1
    @towers = [[3, 2, 1], [], []]
  end #initialize

  def display
    screen_display = ""
    for i in (0...@towers.length)
      output = []
      for j in (0...@towers.length)
        @towers[j][i] ? output << @towers[j][i] : output << "|"
      end
      screen_display = output.join("\t") + "\n" + screen_display
    end
    puts screen_display
    puts "__________________"
    puts "a\tb\tc"
  end #display

  def play
    puts "Welcome to Towers of Hanoi!\nIf you don't know how to play then look it up!\n\n"
    puts "Here is your starting tower:"
    display

    until won?
      puts "Where to move from?"
      input_from = gets.chomp
      unless valid_input?(input_from)
        puts "Invalid input: #{input_from}"
        puts "__________________\n\n"
        next
      end

      puts "Where to move to?"
      input_to = gets.chomp
      unless valid_input?(input_to)
        puts "Invalid input: #{input_to}"
        puts "__________________\n\n"
        next
      end

      from = convert_input(input_from)
      to = convert_input(input_to)
      if valid_move?(from, to)
        move(from, to)
        puts "\n"
        display
        puts "Moved from #{input_from} to #{input_to}\n\n"
      else
        puts "Invalid move from #{input_from} to #{input_to}\n"
        puts "__________________\n\n"
      end

    end #won? loop
    puts "***********************************************"
    puts "CONGRATULATIONS! You have beat Towers of Hanoi!"
    puts "***********************************************"
  end #play

  def render
    #Not sure what this was meant to do
  end #render

  def won?
    #I thought it was a requirement for it all to be moved to the last stack
    #@towers[0].empty? && @towers[1].empty? && @towers[2] == [1,2,3]
    @towers[0].empty? &&
    ((@towers[1].empty? && @towers[2] == [3,2,1]) ||
    (@towers[1] == [3,2,1] && @towers[2].empty?))
  end #won?

  #from_tower & to_tower will be integers from 0-2
  def valid_move?(from_tower, to_tower)
    if @towers[from_tower][-1] && @towers[to_tower].length < @towers.length
      if @towers[to_tower].empty?
        return true
      else
        return @towers[from_tower][-1] < @towers[to_tower][-1]
      end
    else
      return false
    end #wanted to use ?: but it turned out to be pretty long
  end #valid_move?

  def move(from_tower, to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end #move

  def valid_input?(input)
    valid = ("a".."c").to_a
    valid.include?(input)
  end #

  def convert_input(input)
    input.ord - "a".ord
  end #convert_input
end #class TowersOfHanoi
